# BowncerBot

[![pipeline status](https://gitlab.com/kipters/bouncerbot/badges/master/pipeline.svg)](https://gitlab.com/kipters/bouncerbot/-/commits/master)

A bot that automatically kicks spammers in Telegram chats
