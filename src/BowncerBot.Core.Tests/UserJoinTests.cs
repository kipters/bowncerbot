using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;
using BowncerBot.Core.Tests.Attributes;
using Microsoft.Extensions.Logging;
using Moq;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Xunit;

namespace BowncerBot.Core.Tests
{
    public class UserJoinTests
    {
        private Mock<ITelegramBotClient> Bot { get; } = new Mock<ITelegramBotClient>();
        private Mock<ILogger<BotEngine>> Logger { get; } = new Mock<ILogger<BotEngine>>();
        private Mock<IUserMessageService> UserMessage { get; } = new Mock<IUserMessageService>();
        private Mock<IKnownChatRepository> ChatRepository { get; } = new Mock<IKnownChatRepository>();
        private Mock<IBlacklistService> Blacklist { get; } = new Mock<IBlacklistService>();

        private readonly int _botId;

        public UserJoinTests()
        {
            var rnd = new Random();
            _botId = rnd.Next();

            Bot
                .Setup(b => b.GetMeAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User { Id = _botId, IsBot = true });
        }

        [Theory, AutoMoqData]
        public async Task UserIdIsCheckedAgainstBlacklist(long chatId, int userId)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ReturnsAsync(false)
                .Verifiable();

            var msg = new Message
            {
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            Blacklist.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task BlacklistedUserIsExpelledForever(long chatId, int userId)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ReturnsAsync(true);

            Bot
                .Setup(b => b.KickChatMemberAsync(It.Is<ChatId>(c => c.Identifier == chatId), userId, default, It.IsAny<CancellationToken>()))
                .Verifiable();

            var msg = new Message
            {
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task WarningIsPostedIfKickIsDenied(long chatId, int userId, int messageId, string failMessage)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ReturnsAsync(true);

            UserMessage
                .Setup(u => u.GetFailedToKickMessage())
                .ReturnsAsync(failMessage)
                .Verifiable();

            Bot
                .Setup(b => b.KickChatMemberAsync(It.Is<ChatId>(c => c.Identifier == chatId), userId, default, It.IsAny<CancellationToken>()))
                .ThrowsAsync(new ApiRequestException("Bad Request: CHAT_ADMIN_REQUIRED"));

            Bot
                .Setup(b => b.SendTextMessageAsync(It.Is<ChatId>(c => c.Identifier == chatId), failMessage, It.IsAny<ParseMode>(), It.IsAny<bool>(), It.IsAny<bool>(), messageId, It.IsAny<IReplyMarkup>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Message())
                .Verifiable();

            var msg = new Message
            {
                MessageId = messageId,
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            UserMessage.VerifyAll();
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task UserIsGreetedInTheirOwnLanguage(long chatId, int userId, int messageId, string message, string languageCode)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ReturnsAsync(false);

            Bot
                .Setup(b => b.SendTextMessageAsync(It.Is<ChatId>(c => c.Identifier == chatId), message, It.IsAny<ParseMode>(), It.IsAny<bool>(), It.IsAny<bool>(), messageId, It.IsAny<IReplyMarkup>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Message())
                .Verifiable();

            UserMessage
                .Setup(u => u.GetWelcomeMessage(languageCode))
                .ReturnsAsync(message)
                .Verifiable();

            var msg = new Message
            {
                MessageId = messageId,
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false, LanguageCode = languageCode }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            UserMessage.VerifyAll();
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task UserIsGreetedInNeutralLanguageIfUnknown(long chatId, int userId, int messageId, string defaultMessage)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ReturnsAsync(false);

            Bot
                .Setup(b => b.SendTextMessageAsync(It.Is<ChatId>(c => c.Identifier == chatId), defaultMessage, It.IsAny<ParseMode>(), It.IsAny<bool>(), It.IsAny<bool>(), messageId, It.IsAny<IReplyMarkup>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Message())
                .Verifiable();

            UserMessage
                .Setup(u => u.GetWelcomeMessage(null))
                .ReturnsAsync(defaultMessage)
                .Verifiable();

            var msg = new Message
            {
                MessageId = messageId,
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false, LanguageCode = null }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            UserMessage.VerifyAll();
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task WarnGroupIfCheckFails(long chatId, int userId, int messageId, string message)
        {
            //Given
            Blacklist
                .Setup(b => b.IsUserInBlacklist(userId))
                .ThrowsAsync(new Exception());

            Bot
                .Setup(b => b.SendTextMessageAsync(It.Is<ChatId>(c => c.Identifier == chatId), message, It.IsAny<ParseMode>(), It.IsAny<bool>(), It.IsAny<bool>(), messageId, It.IsAny<IReplyMarkup>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Message())
                .Verifiable();

            UserMessage
                .Setup(u => u.GetFailedToCheckMessage())
                .ReturnsAsync(message)
                .Verifiable();

            var msg = new Message
            {
                MessageId = messageId,
                Chat = new Chat { Id = chatId },
                NewChatMembers = new[]
                {
                    new User { Id = userId, IsBot = false, LanguageCode = null }
                }
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            UserMessage.VerifyAll();
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task AllJoiningUsersAreChecked(long chatId, int[] userIds)
        {
            //Given
            foreach (var userId in userIds)
            {
                Blacklist
                    .Setup(b => b.IsUserInBlacklist(userId))
                    .ReturnsAsync(false)
                    .Verifiable();
            }

            var msg = new Message
            {
                Chat = new Chat { Id = chatId },
                NewChatMembers = userIds
                    .Select(id => new User
                    {
                        Id = id,
                        IsBot = false
                    })
                    .ToArray()
            };

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            //When
            await sut.HandleMessage(msg);

            //Then
            Blacklist.VerifyAll();
        }
    }
}
