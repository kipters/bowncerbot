using System;
using System.Threading;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;
using BowncerBot.Core.Tests.Attributes;
using Microsoft.Extensions.Logging;
using Moq;
using Telegram.Bot;
using Telegram.Bot.Types;
using Xunit;

namespace BowncerBot.Core.Tests
{
    public class BotLeaveTests
    {
        private Mock<ITelegramBotClient> Bot { get; } = new Mock<ITelegramBotClient>();
        private Mock<ILogger<BotEngine>> Logger { get; } = new Mock<ILogger<BotEngine>>();
        private Mock<IUserMessageService> UserMessage { get; } = new Mock<IUserMessageService>();
        private Mock<IKnownChatRepository> ChatRepository { get; } = new Mock<IKnownChatRepository>();
        private Mock<IBlacklistService> Blacklist { get; } = new Mock<IBlacklistService>();

        private readonly int _botId;

        public BotLeaveTests()
        {
            var rnd = new Random();
            _botId = rnd.Next();

            Bot
                .Setup(b => b.GetMeAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User { Id = _botId, IsBot = true });
        }

        [Theory, AutoMoqData]
        public async Task BotDeletesGroupInfoFromRepositoryAsync(long chatId)
        {
            //Given
            ChatRepository
                .Setup(b => b.PurgeChat(chatId))
                .Returns(new ValueTask())
                .Verifiable();

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            var msg = new Message
            {
                Chat = new Chat { Id = chatId },
                LeftChatMember = new User { Id = _botId, IsBot = true }
            };

            //When
            await sut.HandleMessage(msg);

            //Then
            ChatRepository.VerifyAll();
        }
    }
}
