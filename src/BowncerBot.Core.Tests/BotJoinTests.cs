using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;
using BowncerBot.Core.Tests.Attributes;
using Microsoft.Extensions.Logging;
using Moq;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Xunit;

namespace BowncerBot.Core.Tests
{
    public class BotJoinTests
    {
        private Mock<ITelegramBotClient> Bot { get; } = new Mock<ITelegramBotClient>();
        private Mock<ILogger<BotEngine>> Logger { get; } = new Mock<ILogger<BotEngine>>();
        private Mock<IUserMessageService> UserMessage { get; } = new Mock<IUserMessageService>();
        private Mock<IKnownChatRepository> ChatRepository { get; } = new Mock<IKnownChatRepository>();
        private Mock<IBlacklistService> Blacklist { get; } = new Mock<IBlacklistService>();

        private readonly int _botId;

        public BotJoinTests()
        {
            var rnd = new Random();
            _botId = rnd.Next();

            Bot
                .Setup(b => b.GetMeAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User { Id = _botId, IsBot = true });
        }

        [Theory, AutoMoqData]
        public async Task BotDisplaysSplashMessage(long chatId, string splashMessage)
        {
            //Given
            Bot
                .Setup(b => b.SendTextMessageAsync(It.Is<ChatId>(c => c.Identifier == chatId), splashMessage, It.IsAny<ParseMode>(), It.IsAny<bool>(), It.IsAny<bool>(), 0, It.IsAny<IReplyMarkup>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Message())
                .Verifiable();

            UserMessage
                .Setup(b => b.GetSplashMessage())
                .ReturnsAsync(splashMessage);

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            var msg = new Message
            {
                Chat = new Chat
                {
                    Id = chatId
                },
                NewChatMembers = new[]
                {
                    new User { Id = _botId, IsBot = true }
                }
            };

            //When
            await sut.HandleMessage(msg);

            //Then
            Bot.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task BotStoresChatIdInKnownChatsRepoAsync(long chatId)
        {
            //Given
            ChatRepository
                .Setup(b => b.StoreChat(chatId))
                .Returns(new ValueTask())
                .Verifiable();

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            var msg = new Message
            {
                Chat = new Chat
                {
                    Id = chatId
                },
                NewChatMembers = new[]
                {
                    new User { Id = _botId, IsBot = true }
                }
            };

            //When
            await sut.HandleMessage(msg);

            //Then
            ChatRepository.VerifyAll();
        }

        [Theory, AutoMoqData]
        public async Task BotUpdatesAdminListAsync(long chatId, ChatMember[] admins)
        {
            //Given
            Bot
                .Setup(b => b.GetChatAdministratorsAsync(It.Is<ChatId>(c => c.Identifier == chatId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(admins)
                .Verifiable();

            var adminIds = admins.Select(a => a.User.Id);

            ChatRepository
                .Setup(r => r.StoreAdminIds(chatId, adminIds))
                .Verifiable();

            var sut = new BotEngine(Logger.Object, Bot.Object, UserMessage.Object, ChatRepository.Object, Blacklist.Object);
            await sut.Initialize();

            var msg = new Message
            {
                Chat = new Chat
                {
                    Id = chatId
                },
                NewChatMembers = new[]
                {
                    new User { Id = _botId, IsBot = true }
                }
            };

            //When
            await sut.HandleMessage(msg);

            //Then
            Bot.VerifyAll();
            ChatRepository.VerifyAll();
        }
    }
}
