using System.Collections.Generic;

namespace BowncerBot.Poll.Config
{
    public class UserMessagesConfig
    {
        public string? SplashMessage { get; set; }
        public Dictionary<string, string>? WelcomeMessages { get; set; }
        public string DefaultLanguageCode { get; set; } = "en-US";
        public string? FailedToKickUserMessage { get; set; }
        public string? FailedToCheckUserMessage { get; set; }
    }
}
