﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BowncerBot.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace BowncerBot.Poll
{
    public class BotWorker : IHostedService
    {
        private readonly ILogger<BotWorker> _logger;
        private readonly ITelegramBotClient _bot;
        private readonly BotEngine _engine;

        public BotWorker(ILogger<BotWorker> logger, ITelegramBotClient bot, BotEngine engine)
        {
            _logger = logger;
            _bot = bot;
            _engine = engine;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Worker starting");
            await _engine.Initialize();

            _bot.OnMessage += OnMessage;
            _bot.StartReceiving();

            _logger.LogInformation("Worker started");
        }

        private void OnMessage(object? sender, MessageEventArgs e)
        {
            var message = e.Message;
            _logger.LogDebug("Received message from {userId} on chat {chatId}", e.Message.From.Id, e.Message.Chat.Id);

            _engine.HandleMessage(message);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Worker stopping");

            _bot.StopReceiving();
            _bot.OnMessage -= OnMessage;

            _logger.LogInformation("Worker stopped");

            return Task.CompletedTask;
        }
    }
}
