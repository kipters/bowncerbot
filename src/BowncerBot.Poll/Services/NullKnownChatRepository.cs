﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;

namespace BowncerBot.Poll.Services
{
    public class NullKnownChatRepository : IKnownChatRepository
    {
        public ValueTask PurgeChat(long chatId) => new ValueTask();

        public ValueTask StoreAdminIds(long chatId, IEnumerable<int> adminIds) => new ValueTask();

        public ValueTask StoreChat(long chatId) => new ValueTask();
    }
}
