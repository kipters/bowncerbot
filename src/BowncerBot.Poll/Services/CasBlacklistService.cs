﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;

namespace BowncerBot.Poll.Services
{
    public class CasBlacklistService : IBlacklistService
    {
        private readonly HttpClient _client;

        public CasBlacklistService()
        {
            _client = new HttpClient { BaseAddress = new Uri("https://api.cas.chat/") };
        }

        public async ValueTask<bool> IsUserInBlacklist(int userId)
        {
            var response = await _client.GetStreamAsync($"check?user_id={userId}");
            using var json = await JsonDocument.ParseAsync(response);

            foreach (var prop in json.RootElement.EnumerateObject())
            {
                if (prop.Name != "ok")
                {
                    continue;
                }

                return prop.Value.GetBoolean();
            }

            throw new InvalidOperationException("Could not check user on CAS (missing 'ok' in response)");
        }
    }
}
