using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;
using BowncerBot.Poll.Config;
using Microsoft.Extensions.Options;

namespace BowncerBot.Poll.Services
{
    public class ConfigUserMessageService : IUserMessageService
    {
        private readonly string _defaultWelcomeMessage;
        private readonly string _splashMessage;
        private readonly Dictionary<string, string> _welcomeMessages;
        private readonly string _kickFailMessage;
        private readonly string _checkFailMessage;

        public ConfigUserMessageService(IOptions<UserMessagesConfig> options)
        {
            if (options.Value is null)
            {
                throw new ArgumentNullException("You must bind a configuration section for UserMessagesConfig");
            }

            var cfg = options.Value;

            if (string.IsNullOrWhiteSpace(cfg.SplashMessage))
            {
                throw new ArgumentException("Missing Splash Message");
            }

            if (cfg.WelcomeMessages is null)
            {
                throw new ArgumentNullException("Missing welcome messages node");
            }

            if (cfg.WelcomeMessages.Count == 0)
            {
                throw new ArgumentException("There are no welcome messages registered");
            }

            var defaultLanguage = cfg.DefaultLanguageCode;

            if (defaultLanguage is null)
            {
                throw new ArgumentNullException("Default language code not defined");
            }

            if (!cfg.WelcomeMessages.TryGetValue(defaultLanguage, out var defaultWelcomeMessage))
            {
                throw new ArgumentException("Missing welcome message for default language");
            }

            if (string.IsNullOrWhiteSpace(cfg.FailedToKickUserMessage))
            {
                throw new ArgumentException("Missing message for kick failures");
            }

            if (string.IsNullOrWhiteSpace(cfg.FailedToCheckUserMessage))
            {
                throw new ArgumentException("Missing message for check failure");
            }

            _defaultWelcomeMessage = defaultWelcomeMessage;
            _splashMessage = cfg.SplashMessage;
            _welcomeMessages = cfg.WelcomeMessages;
            _kickFailMessage = cfg.FailedToKickUserMessage;
            _checkFailMessage = cfg.FailedToCheckUserMessage;
        }

        public ValueTask<string> GetFailedToCheckMessage() => new ValueTask<string>(_checkFailMessage);

        public ValueTask<string> GetFailedToKickMessage() => new ValueTask<string>(_kickFailMessage);

        public ValueTask<string> GetSplashMessage() => new ValueTask<string>(_splashMessage);

        public ValueTask<string> GetWelcomeMessage(string? languageCode)
        {
            if (languageCode is null)
            {
                return new ValueTask<string>(_defaultWelcomeMessage);
            }

            if (_welcomeMessages.TryGetValue(languageCode, out var message))
            {
                return new ValueTask<string>(message);
            }

            return new ValueTask<string>(_defaultWelcomeMessage);
        }
    }
}
