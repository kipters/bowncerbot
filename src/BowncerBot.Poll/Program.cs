using BowncerBot.Core;
using BowncerBot.Core.Interfaces;
using BowncerBot.Poll.Config;
using BowncerBot.Poll.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Telegram.Bot;

namespace BowncerBot.Poll
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    builder.AddUserSecrets<Program>();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var cfg = hostContext.Configuration;
                    services.Configure<TelegramConfig>(cfg.GetSection("Telegram"));
                    services.Configure<UserMessagesConfig>(cfg.GetSection("UserMessages"));

                    services.AddSingleton<IUserMessageService, ConfigUserMessageService>();
                    services.AddSingleton<IKnownChatRepository, NullKnownChatRepository>();
                    services.AddSingleton<IBlacklistService, CasBlacklistService>();

                    services.AddSingleton<ITelegramBotClient>(s =>
                    {
                        var c = s.GetRequiredService<Microsoft.Extensions.Configuration.IConfiguration>();
                        var config = s.GetRequiredService<IOptions<TelegramConfig>>();
                        var client = new TelegramBotClient(config.Value.BotToken);
                        return client;
                    });

                    services.AddSingleton<BotEngine>();

                    services.AddHostedService<BotWorker>();
                });
    }
}
