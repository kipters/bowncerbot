using System;
using System.Linq;
using System.Threading.Tasks;
using BowncerBot.Core.Interfaces;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;

namespace BowncerBot.Core
{
    public class BotEngine
    {
        private readonly ILogger<BotEngine> _logger;
        private readonly ITelegramBotClient _bot;
        private readonly IUserMessageService _userMessage;
        private readonly IKnownChatRepository _chatRepository;
        private readonly IBlacklistService _blacklist;
        private int _id;

        public BotEngine(ILogger<BotEngine> logger
            , ITelegramBotClient bot
            , IUserMessageService userMessage
            , IKnownChatRepository chatRepository
            , IBlacklistService blacklist
        )
        {
            _logger = logger;
            _bot = bot;
            _userMessage = userMessage;
            _chatRepository = chatRepository;
            _blacklist = blacklist;
        }

        public async ValueTask Initialize()
        {
            var me = await _bot.GetMeAsync();
            _logger.LogInformation("Initialized bot {name} (@{username}) with id {id}", me.FirstName, me.Username, me.Id);
            _id = me.Id;
        }

        public ValueTask HandleMessage(Message message)
        {
            if (message.NewChatMembers != null && message.NewChatMembers.Any())
            {
                return HandleJoins(message);
            }

            if (!(message.LeftChatMember is null))
            {
                return HandleLeave(message);
            }

            return new ValueTask();
        }

        private ValueTask HandleLeave(Message message)
        {
            if (message.LeftChatMember.Id == _id)
            {
                return HandleLostChat(message);
            }

            return new ValueTask();
        }

        private async ValueTask HandleLostChat(Message message)
        {
            await _chatRepository.PurgeChat(message.Chat.Id);
        }

        private async ValueTask HandleJoins(Message message)
        {
            var newChatMembers = message.NewChatMembers;

            if (newChatMembers.Length == 1 && newChatMembers[0].Id == _id)
            {
                await HandleNewChat(message);
                return;
            }

            foreach (var user in message.NewChatMembers)
            {
                await HandleNewUser(message.Chat.Id, message.MessageId, user);
            }
        }

        private async ValueTask HandleNewUser(long chatId, int messageId, User user)
        {
            var shouldBeBanned = true;
            try
            {
                shouldBeBanned = await _blacklist.IsUserInBlacklist(user.Id);
            }
            catch
            {
                var checkFailMessage = await _userMessage.GetFailedToCheckMessage();
                await _bot.SendTextMessageAsync(chatId, checkFailMessage, replyToMessageId: messageId);
                return;
            }

            if (shouldBeBanned)
            {
                try
                {
                    await _bot.KickChatMemberAsync(chatId, user.Id);
                }
                catch (ApiRequestException e) when (e.Message.Contains("CHAT_ADMIN_REQUIRED"))
                {
                    var msg = await _userMessage.GetFailedToKickMessage();
                    await _bot.SendTextMessageAsync(chatId, msg, replyToMessageId: messageId);
                }

                return;
            }

            var welcomeMessage = await _userMessage.GetWelcomeMessage(user.LanguageCode);
            await _bot.SendTextMessageAsync(chatId, welcomeMessage, replyToMessageId: messageId);
        }

        private async ValueTask HandleNewChat(Message message)
        {
            var chatId = message.Chat.Id;
            var splashMessage = await _userMessage.GetSplashMessage();

            var admins = await _bot.GetChatAdministratorsAsync(chatId);

            await _chatRepository.StoreChat(chatId);
            await _chatRepository.StoreAdminIds(chatId, admins.Select(x => x.User.Id));
            await _bot.SendTextMessageAsync(chatId, splashMessage);
        }
    }
}
