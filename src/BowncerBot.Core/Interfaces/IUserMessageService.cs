using System.Threading.Tasks;

namespace BowncerBot.Core.Interfaces
{
    public interface IUserMessageService
    {
        ValueTask<string> GetSplashMessage();
        ValueTask<string> GetWelcomeMessage(string? languageCode);
        ValueTask<string> GetFailedToKickMessage();
        ValueTask<string> GetFailedToCheckMessage();
    }
}
