using System.Threading.Tasks;

namespace BowncerBot.Core.Interfaces
{
    public interface IBlacklistService
    {
        ValueTask<bool> IsUserInBlacklist(int userId);
    }
}
