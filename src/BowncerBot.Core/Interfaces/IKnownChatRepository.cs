using System.Collections.Generic;
using System.Threading.Tasks;

namespace BowncerBot.Core.Interfaces
{
    public interface IKnownChatRepository
    {
        ValueTask StoreChat(long chatId);
        ValueTask StoreAdminIds(long chatId, IEnumerable<int> adminIds);

        ValueTask PurgeChat(long chatId);
    }
}
