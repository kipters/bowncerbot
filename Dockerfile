FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster as build-env

WORKDIR /projects

COPY ./src/**/*.csproj ./
COPY ./src/Directory.Build.props ./
ARG runtime_id=alpine-x64

RUN find . -type f -name '*.csproj' -exec \
    dotnet restore --runtime ${runtime_id} {} \; \
    && rm -rf /projects

WORKDIR /app
ARG build_id
ARG commit_id

COPY BowncerBot.sln .
COPY ./src/ ./src
ARG app_name

RUN dotnet publish \
    --configuration=Release \
    --output=out \
    --runtime=${runtime_id} \
    --version-suffix=alpine-docker \
    -p:BuildId=${build_id} \
    -p:SourceRevisionId=${commit_id} \
    src/${app_name}

FROM mcr.microsoft.com/dotnet/core/runtime-deps:3.1-alpine3.10
ARG app_name
COPY --from=build-env /app/out /app
RUN mv /app/${app_name} /app/entrypoint
WORKDIR /app
ENTRYPOINT [ "/app/entrypoint" ]
